package com.devcamp.task52s50;

// Import  thư viện
import java.util.Locale;
import java.util.Arrays;
import java.util.Date;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;


public class Order {
    
    int id; // id của order
    String customerName; // tên khách hàng
    long price;// tổng giá tiền
    Date orderDate; // ngày thực hiện order
    boolean confirm; // đã xác nhận hay chưa?
    String[] items; // danh sách mặt hàng đã mua

    

    // khởi tạo 1 tham số
    public Order (String name) {
        this.id = 1;
        this.customerName = name;
        this.price = 20000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] { "book", "pen", "rule" };
    }
     // khởi tạo nhiều tham số
     public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = true;
        this.items = new String[] { "book", "pen", "rule" };
    }
     
    // khởi tạo 0 tham số
    public Order() {
        this("2");
       
    }
    // Khởi tạo 3 tham số
    public Order(int id,String customerName,long price){
        this(id,customerName,price,new Date(),true,new String[] { "toan", "van", "huong" });
    }
  

    @Override
    public String toString() {

        // Định dạng tiêu chuẩn Việt Nam
        Locale.setDefault(new Locale("vi", "VN"));
        // Định dạng cho ngày tháng
        String pattern = "dd-MM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern(pattern);
        // Định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);

        return
        "order [id = "+id
        +", customerName = "+ customerName
        +", price = "+ usNumberFormat.format(price) 
        +", orderDate = "+ myFormatObj.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
        +", confirm = "+ confirm
        +", items = "+ Arrays.toString(items) + "]";
    }

}
