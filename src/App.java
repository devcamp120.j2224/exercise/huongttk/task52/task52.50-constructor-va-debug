import java.util.ArrayList;
import java.util.Date;

import com.devcamp.task52s50.Order;

public class App {
    public static void main(String[] args) throws Exception {
       
        ArrayList<Order> array = new ArrayList<Order>();

        Order order1 = new Order();
        Order order2 = new Order("Minh Duy");
        Order order3 = new Order(11, "Phomai", 160000);
        Order order4 = new Order(115, "Thanh Tra", 30000, new Date(), false, new String[] {"iphone", "samsung"});

        array.add(order1);
        array.add(order2);
        array.add(order3);
        array.add(order4);

        // In ra màn hình
        for (Order order : array){
            System.out.println(order.toString());
        }
    }
}
